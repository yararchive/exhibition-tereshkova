$(document).ready(function() {
  hs.align = 'center';
  hs.graphicsDir = 'js/vendor/highslide/graphics/';
  hs.wrapperClassName = 'wide-border';

  $(".outline-link a").click(function() {
    $(".outline-bg").fadeToggle("400");
  });

  $('.blur, .outline-bg').blurjs({ cache: true, source: 'body', radius: 10, overlay: 'rgba(81,112,132,0.1)' });

  loaderTexts = new Array("Загружаю документы", "Загрузка может занять продолжительное время", "Пожалуйста, подождите");
  textsCount = textNumber =loaderTexts.length;

  changeLoaderText = function() {
    if ($("#loader-screen").css("display") === "block") {
      textNumber++;
      newText = loaderTexts[textNumber % textsCount]
      console.log("Loader text changed to: " + newText);
      $("#loader-text").fadeOut(500, function() {
        $("#loader-text").text(newText).fadeIn(500);
      });
      setTimeout(changeLoaderText, 5000);
    }
  };
  setTimeout(changeLoaderText, 5000);

  $(window).load(function() {
    $("#loader-screen").fadeOut("500");
  });
});